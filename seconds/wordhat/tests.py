from django.test import TestCase
from django.urls import reverse
from unittest import mock

# Create your tests here.
from .models import Player, WordHat, Word, Play, Round

def makePlayer(id=1, name="Ben"):
    player = Player(id=id, name=name)
    player.save()
    return player

def makeWordHat(id=1, hatname="Cool name"):
    wordhat = WordHat(id=id, hatname=hatname)
    wordhat.save()
    return wordhat

def addWords(player, wordhat, words):
    for word in words:
        Word(submitted_by=player, wordhat=wordhat, word=word).save()

class ViewTests(TestCase):

    def test_addPlayerGet(self):
        response = self.client.get(reverse('add_player'))
        self.assertIn('<form id="new_player" action="add_player" method="post" > ',
                      response.content.decode('utf-8'))

    def test_addPlayerPost(self):
        player_name = "Sarah"
        response = self.client.post(reverse('add_player'),
                                    {'player_name' : player_name})

        # assert no throw
        player = Player.objects.get(name=player_name) # make sure this doesn't fail, so it is added

        self.assertIn(f'player_id={player.id}', response.url)

    def test_playerPage(self):
        player = makePlayer(name="Sarah")
        wordhat1 = makeWordHat(1, "FirstHat")
        wordhat2 = makeWordHat(2, "SecondHat")
        wordhat3 = makeWordHat(3, "ThirdHat")
        player.wordhats.add(wordhat1)
        player.wordhats.add(wordhat2)
        player.wordhats.add(wordhat3)
        response = self.client.get(reverse('player_page'),
                                   {'player_id' : player.id})
        html = response.content.decode('utf8')

        self.assertIn(player.name, html)
        self.assertIn(wordhat1.hatname, html)
        self.assertIn(wordhat2.hatname, html)
        self.assertIn(wordhat3.hatname, html)

        # TODO: test generated urls?



    def test_newWordHatGet(self):

        player_id=6
        response = self.client.get(reverse('add_wordhat'),
                                   {'player_id' : player_id})
        html = response.content.decode('utf-8')
        self.assertIn(f'value={player_id}', html)

        # TODO somehow verify what is in the form?

    def test_newWordHatPost(self):

        old_hat = "SuperHat"
        player = makePlayer(name="Bob")
        wordhat1 = makeWordHat(hatname=old_hat)
        player.wordhats.add(wordhat1)

        new_wordhat = "BestHat"
        response = self.client.post(reverse('add_wordhat'),
                                    {'wordhat_name' : new_wordhat,
                                    'player_id' : player.id})

        player = Player.objects.get(id=player.id)

        # Assert no throw, wordhats should exist in this set
        player.wordhats.get(hatname=old_hat)
        player.wordhats.get(hatname=new_wordhat)

    def test_connectToWordHatExisting(self):
        hatname = "SomeUniqueName"
        wordhat = makeWordHat(hatname=hatname)
        player = makePlayer()
        response = self.client.post(reverse('connect_wordhat'),
                                    {'wordhat_name' : hatname,
                                    'player_id' : player.id})

        player = Player.objects.get(id=player.id)

        # Assert no throw, wordhat should now be in this set
        player.wordhats.get(hatname=hatname)

    def test_submitWords(self):
        player_id = 2
        player = makePlayer(player_id)
        wordhat_id = 3
        wordhat = makeWordHat(wordhat_id)

        words = ["test", "some", "words"]
        addWords(player, wordhat, words)

        response = self.client.get(f"{reverse('submit_words')}?player_id={player_id}&wordhat_id={wordhat_id}")
        html = response.content.decode('utf-8')

        self.assertTrue(f'<input id="player" name="player" type="hidden" value={player_id}>' in html)
        self.assertTrue(f'<input id="wordhat" name="wordhat" type="hidden" value={wordhat_id}>' in html)

        for word in words:
            self.assertIn(word, html)

    def test_submitWord(self):
        player_id = 2
        player = makePlayer(player_id)
        wordhat_id = 3
        wordhat = makeWordHat(wordhat_id)

        word = "foo"
        response = self.client.post(reverse('submit_word'),
                                    {'wordhat' : wordhat_id, 'player' : player_id, 'new_word' : word})

        wordobj = Word.objects.get(word=word)
        self.assertEqual(wordobj.submitted_by, player)
        self.assertEqual(wordobj.wordhat, wordhat)

    @mock.patch('wordhat.models.WordHat.get_player_scores')
    def test_resutls(self, mock_get_player_scores):

        mock_get_player_scores.return_value = {"Jamie" : 5, "Rosa" : 10, "Gillard" : 0}

        wordhat = makeWordHat()

        response = self.client.get(reverse('results'),
                                   {'wordhat_id' : wordhat.id})

        html = response.content.decode('utf-8')

        for name, score in mock_get_player_scores.return_value.items():
            self.assertIn(name, html)
            self.assertIn(str(score), html)

        #TODO: see if the score matches

class ModelTests(TestCase):

    def test_getAllWords_returnsAllWords(self):
        wordhat = self._make_wordlist()

        words = wordhat.get_all_words()

        for i, item in enumerate(self.wordlist):
            self.assertTrue(item in words)

    def test_getRandomWord_returnsRandomWord(self):
        wordhat = self._make_wordlist()

        words = Word.objects.all()

        first_random = wordhat.get_random_word()
        print(first_random)
        self.assertTrue(first_random in words)

        second_random = wordhat.get_random_word()
        print(second_random)
        count = 0
        while (first_random == second_random):
            second_random = wordhat.get_random_word()
            print(second_random)

            count += 1

            if count > 10: # statistcally impossible to pick 10 times the same??
                self.assertTrue(False) # TODO: make better fail condition

        item = wordhat.get_random_word()

    def test_getUnplayedWords(self):

        # SETUP
        wordhat = self._make_wordlist()
        wordhat.start_round_if_not_started()
        words = Word.objects.all()
        wordhat.add_play_to_round(word=words[0], answered_by=self.player1, answered_correctly=True)
        wordhat.add_play_to_round(word=words[1], answered_by=self.player1, answered_correctly=True)

        # TEST
        unplayed = wordhat.get_unplayed_words()

        self.assertNotIn(words[0], unplayed)
        self.assertNotIn(words[1], unplayed)
        self.assertEqual(len(unplayed), len(words) - 2)

    def test_getPlayerScores(self):

        player1 = makePlayer(id=1, name="Florence")
        player2 = makePlayer(id=2, name="Ford")
        wordhat = makeWordHat()

        addWords(player1, wordhat, ["Hippo", "Chicken", "House"])
        addWords(player2, wordhat, ["Bathroom", "Doggie", "Slipper"])

        wordhat.start_round_if_not_started()

        word = wordhat.get_random_word()
        wordhat.add_play_to_round(word=word, answered_by=player1, answered_correctly=True)
        word = wordhat.get_random_word()
        wordhat.add_play_to_round(word=word, answered_by=player1, answered_correctly=True)
        word = wordhat.get_random_word()
        wordhat.add_play_to_round(word=word, answered_by=player2, answered_correctly=True)
        word = wordhat.get_random_word()
        wordhat.add_play_to_round(word=word, answered_by=player2, answered_correctly=False)

        scores = wordhat.get_player_scores()

        self.assertEqual(scores[player1.name], 2)
        self.assertEqual(scores[player2.name], 1)

    def test_getByPlayer_returnsByPlayer(self):
        wordhat = self._make_wordlist()

        player_words = [x for x in wordhat.get_words_by_player(self.player1)]

        words_by_player = []
        for word in self.wordlist:
            if word.submitted_by == self.player1:
                words_by_player.append(word)

        self.assertEqual(sorted(player_words, key=lambda word: word.id), sorted(words_by_player, key=lambda word: word.id))

    def _make_wordlist(self):

        wordhat = WordHat()
        wordhat.save()

        self.player1 = Player()
        self.player1.save()
        self.player2 = Player()
        self.player2.save()

        self.wordlist = []

        word = Word(wordhat = wordhat, word = "random", submitted_by = self.player1)
        word.save()
        self.wordlist.append(word)

        word2 = Word(wordhat = wordhat, word = "word", submitted_by = self.player1)
        word2.save()
        self.wordlist.append(word2)

        word3 = Word(wordhat = wordhat, word = "three", submitted_by = self.player2)
        word3.save()
        self.wordlist.append(word3)

        word4 = Word(wordhat = wordhat, word = "four", submitted_by = self.player2)
        word4.save()
        self.wordlist.append(word4)

        return wordhat
