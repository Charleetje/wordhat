from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('add_player', views.add_player, name='add_player'),
    path('add_wordhat', views.add_wordhat, name='add_wordhat'),
    path('connect_wordhat', views.connect_wordhat, name='connect_wordhat'),
    path('player_page', views.player_page, name='player_page'),
    path('submit_words', views.submit_words, name='submit_words'),
    path('submit_word', views.submit_word, name='submit_word'),
    path('get_random_word', views.get_random_word, name='get_random_word'),
    path('play_wordhat', views.play_wordhat, name='play_wordhat'),
    path('mark_correct', views.mark_correct, name='mark_correct'),
    path('mark_incorrect', views.mark_incorrect, name='mark_incorrect'),
    path('results', views.results, name='results'),
    path('new_round', views.new_round, name='new_round'),
]
