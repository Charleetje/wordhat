from django.db import models
import random

# Create your models here.
class WordHat(models.Model):
    hatname = models.CharField(max_length=200)
    active_round = models.ForeignKey('Round', related_name="active_round",
                                      on_delete=models.CASCADE, null=True, blank=True)
    previous_rounds = models.ManyToManyField('Round', related_name="previous_rounds")

    def start_round_if_not_started(self):
        if not self.active_round:
            self.active_round = Round()
            self.active_round.save()
            self.save()

    def start_new_round(self):

        self.previous_rounds.add(self.active_round)
        self.active_round = None
        self.start_round_if_not_started()

    def get_all_words(self):
        return self.word_set.all()

    def get_unplayed_words(self):
        if self.active_round:
            # TODO: decide whether to make lasting variable?
            words_played = [play.word for play in self.active_round.plays.all()]
        else:
            words_played = []

        return [word for word in self.word_set.all() \
                if word not in words_played]

    def get_random_word(self):
        return random.choice(self.get_unplayed_words())

    def get_words_by_player(self, player):
        return self.word_set.filter(submitted_by=player)

    def add_play_to_round(self, word, answered_by, answered_correctly):
        play = Play(word=word, answered_by=answered_by, answered_correctly=answered_correctly)
        play.save()
        self.active_round.plays.add(play)

    def get_player_scores(self):

        player_scores = {}
        for play in self.active_round.plays.all():
            print(play)
            if play.answered_correctly:
                name = play.answered_by.name

                if name in player_scores:
                    player_scores[name] += 1
                else:
                    player_scores[name] = 1

        return player_scores

    def __str__(self):
        return f'{self.hatname}'

class Player(models.Model):
    name = models.CharField(max_length=50)
    wordhats = models.ManyToManyField(WordHat)

    def __str__(self):
        return f'{self.name}'

class Word(models.Model):

    wordhat = models.ForeignKey(WordHat, on_delete=models.CASCADE)
    word = models.CharField(max_length=50)
    submitted_by = models.ForeignKey(Player, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return f'{self.word} ({self.wordhat}, {self.submitted_by})'

class Play(models.Model):
    word = models.ForeignKey(Word, on_delete=models.CASCADE)
    answered_by = models.ForeignKey(Player, on_delete=models.CASCADE)
    answered_correctly = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.word}, {self.answered_by}, {self.answered_correctly}'

class Round(models.Model):
    plays = models.ManyToManyField(Play)
    time_started = models.DateTimeField(null=True, blank=True)
    time_finished = models.DateTimeField(null=True, blank=True)
