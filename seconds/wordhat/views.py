from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse

from .models import WordHat, Word, Player

def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")

def add_player(request):

    if request.method == "GET":
        return render(request, 'new_player.html')

    if request.method == "POST":
        player_name = request.POST['player_name']
        # TODO: check if unique?
        player = Player(name=player_name)
        player.save()
        url = reverse('player_page')
        return HttpResponseRedirect(f'{url}?player_id={player.id}')

def player_page(request):
    player = Player.objects.get(id=int(request.GET['player_id']))

    context = {
        'player_name' : player.name,
        'wordhat_list' : player.wordhats.all(),
        'edit_wordhat_link_base' : f'{reverse("submit_words")}?player_id={player.id}&wordhat_id=',
        'play_wordhat_link_base' : f'{reverse("play_wordhat")}?player_id={player.id}&wordhat_id=',
        'results_link_base' : f'{reverse("results")}?wordhat_id=',
        'connect_to_wordhat_link' : f'{reverse("connect_wordhat")}?player_id={player.id}',
        'start_new_wordhat_link' : f'{reverse("add_wordhat")}?player_id={player.id}',
    }
    return render(request, 'player_page.html', context)

def add_wordhat(request):

    if request.method == "GET":
        player_id = request.GET['player_id']
        return render(request, 'new_wordhat.html', {"player_id": player_id})

    if request.method == "POST":
        player_id = request.POST['player_id']
        wordhat_name = request.POST['wordhat_name']

        # TODO: check if unique?
        wordhat = WordHat(hatname=wordhat_name)
        wordhat.save()
        player = Player.objects.get(id=player_id)
        player.wordhats.add(wordhat)
        url = reverse('player_page')
        return HttpResponseRedirect(f'{url}?player_id={player_id}')

def connect_wordhat(request):

    if request.method == "GET":
        player_id = request.GET['player_id']
        return render(request, 'connect_wordhat.html', {"player_id": player_id})

    if request.method == "POST":
        player_id = request.POST['player_id']
        wordhat_name = request.POST['wordhat_name']

        wordhat = WordHat.objects.get(hatname=wordhat_name)
        player = Player.objects.get(id=player_id)
        player.wordhats.add(wordhat)

        url = reverse('player_page')
        return HttpResponseRedirect(f'{url}?player_id={player_id}')

def submit_words(request):
    print(request, request.GET)

    # import pdb; pdb.set_trace()

    player = request.GET['player_id']
    Player.objects.get(id=player) # test if it exists
    # Player(id=int(player)).save() # For debugging purposes
    wordhat = request.GET['wordhat_id']
    WordHat.objects.get(id=wordhat) # test if it exists
    # WordHat(id=int(wordhat)).save() # for deubgging purposes

    words = Word.objects.filter(submitted_by=player, wordhat=wordhat)
    print("Existing words", words.all())

    context = {'player' : player,
               'wordhat' : wordhat,
               'user_words' : words.all(),
               'total_word_num' : len(Word.objects.filter(wordhat=wordhat))}

    return render(request, 'submit_words.html', context)

def submit_word(request):

    print("Received word submission, with request: ", request.POST)

    if request.POST:
        player_id = request.POST['player']
        wordhat = request.POST['wordhat']
        word = request.POST['new_word']

        player = Player.objects.get(id=player_id)
        wordhat = WordHat.objects.get(id=wordhat)

        word = Word(wordhat=wordhat, submitted_by=player, word=word)
        word.save()

        return HttpResponse(word)

def get_random_word(request):

    wordhat_id = int(request.GET['wordhat_id'])
    wordhat = WordHat.objects.get(id=wordhat_id)

    try:
        word = wordhat.get_random_word()
        return JsonResponse({'word_id' : word.id, 'word' : word.word})
    except IndexError:
        return JsonResponse({'word_id' : None, 'word' : None})

def play_wordhat(request):

    player_id = request.GET['player_id']

    context = {'player_id' : player_id,
               'wordhat_id' : request.GET['wordhat_id'],
               'playerpage_url' : f"{reverse('player_page')}?player_id={player_id}"
               }

    wordhat = WordHat.objects.get(id=int(context['wordhat_id']))
    # import pdb; pdb.set_trace()
    wordhat.start_round_if_not_started()

    return render(request, 'play_wordhat.html', context)

def mark_played_question(request, answered_correctly):

    player_id = int(request.POST['player_id'])
    player = Player.objects.get(id=player_id)
    word_id = int(request.POST['word_id'])
    word = Word.objects.get(id=word_id)

    print("Marking correct: ", player_id, word_id)

    word.wordhat.add_play_to_round(word=word, answered_by=player, answered_correctly=answered_correctly)

    return HttpResponse({})


def mark_correct(request):

    return mark_played_question(request, True)

def mark_incorrect(request):

    return mark_played_question(request, False)

def results(request):

    wordhat_id = int(request.GET['wordhat_id'])
    wordhat = WordHat.objects.get(id=wordhat_id)

    player_scores = wordhat.get_player_scores()

    return render(request, 'results.html', {'player_list' : [{'name' : name, 'words_guessed' : num} for name, num in player_scores.items()]})

def new_round(request):

    wordhat_id = int(request.POST['wordhat_id'])
    wordhat = WordHat.objects.get(id=wordhat_id)
    wordhat.start_new_round()

    return JsonResponse({'data' : None})
